"use strict";

function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.CustomCircle = void 0;
var _react = _interopRequireWildcard(require("react"));
var _tooltip = require("@nivo/tooltip");
var _colors = require("@nivo/colors");
var _pie = require("@nivo/pie");
function _getRequireWildcardCache(e) { if ("function" != typeof WeakMap) return null; var r = new WeakMap(), t = new WeakMap(); return (_getRequireWildcardCache = function _getRequireWildcardCache(e) { return e ? t : r; })(e); }
function _interopRequireWildcard(e, r) { if (!r && e && e.__esModule) return e; if (null === e || "object" != _typeof(e) && "function" != typeof e) return { "default": e }; var t = _getRequireWildcardCache(r); if (t && t.has(e)) return t.get(e); var n = { __proto__: null }, a = Object.defineProperty && Object.getOwnPropertyDescriptor; for (var u in e) if ("default" !== u && Object.prototype.hasOwnProperty.call(e, u)) { var i = a ? Object.getOwnPropertyDescriptor(e, u) : null; i && (i.get || i.set) ? Object.defineProperty(n, u, i) : n[u] = e[u]; } return n["default"] = e, t && t.set(e, n), n; }
// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars

//https://github.com/plouc/nivo/blob/master/packages/bar/src/BarItem.tsx
var CustomCircle = exports.CustomCircle = function CustomCircle(props) {
  var _props$node$data$cate;
  var _useTooltip = (0, _tooltip.useTooltip)(),
    showTooltipFromEvent = _useTooltip.showTooltipFromEvent,
    hideTooltip = _useTooltip.hideTooltip;
  var getArcColor = (0, _colors.useOrdinalColorScale)({
    scheme: 'category10'
  }, function (v) {
    return v;
  });
  var pie = (0, _pie.usePie)({
    data: ((_props$node$data$cate = props.node.data.categories) === null || _props$node$data$cate === void 0 ? void 0 : _props$node$data$cate.map(function (value, id) {
      return {
        id: id,
        value: value,
        hidden: false,
        data: value,
        color: '',
        formattedValue: "".concat(value),
        label: "".concat(value)
      };
    })) || [],
    radius: props.node.size / 2,
    innerRadius: props.node.size / 2 * 0.7,
    sortByValue: true
  });
  var handleMouseEnter = (0, _react.useCallback)(function (event) {
    var _props$onMouseEnter;
    (_props$onMouseEnter = props.onMouseEnter) === null || _props$onMouseEnter === void 0 || _props$onMouseEnter.call(props, props.node, event);
    //showTooltipFromEvent(renderTooltip(), event)
  }, [props.node, props.onMouseEnter, showTooltipFromEvent /*renderTooltip*/]);
  var handleMouseLeave = (0, _react.useCallback)(function (event) {
    var _props$onMouseLeave;
    (_props$onMouseLeave = props.onMouseLeave) === null || _props$onMouseLeave === void 0 || _props$onMouseLeave.call(props, props.node, event);
    hideTooltip();
  }, [props.node, hideTooltip, props.onMouseEnter]);
  return /*#__PURE__*/_react["default"].createElement("g", {
    transform: "translate(".concat(props.node.x, ",").concat(props.node.y, ")"),
    onMouseEnter: handleMouseEnter,
    onMouseLeave: handleMouseLeave
  }, /*#__PURE__*/_react["default"].createElement("circle", {
    r: props.node.size / 2,
    stroke: "rgb(216, 218, 235)",
    strokeWidth: 12
  }), /*#__PURE__*/_react["default"].createElement("circle", {
    r: props.node.size / 2,
    fill: "rgb(45, 0, 75)",
    stroke: "rgb(45, 0, 75)",
    strokeWidth: 6
  }), pie.dataWithArc.map(function (datum, i) {
    return /*#__PURE__*/_react["default"].createElement("path", {
      key: i,
      d: pie.arcGenerator(datum.arc) || undefined,
      fill: getArcColor(i)
    });
  }), props.node.size > 52 && /*#__PURE__*/_react["default"].createElement("text", {
    fill: "white",
    textAnchor: "middle",
    dominantBaseline: "central",
    style: {
      fontSize: 14,
      fontWeight: 800
    }
  }, props.node.value.toString()));
};