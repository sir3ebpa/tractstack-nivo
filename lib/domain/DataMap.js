"use strict";

Object.defineProperty(exports, "__esModule", {
  value: true
});
exports.RelationType = exports.DataMap = void 0;
function _typeof(o) { "@babel/helpers - typeof"; return _typeof = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function (o) { return typeof o; } : function (o) { return o && "function" == typeof Symbol && o.constructor === Symbol && o !== Symbol.prototype ? "symbol" : typeof o; }, _typeof(o); }
function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }
function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, _toPropertyKey(descriptor.key), descriptor); } }
function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); Object.defineProperty(Constructor, "prototype", { writable: false }); return Constructor; }
function _defineProperty(obj, key, value) { key = _toPropertyKey(key); if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }
function _toPropertyKey(arg) { var key = _toPrimitive(arg, "string"); return _typeof(key) === "symbol" ? key : String(key); }
function _toPrimitive(input, hint) { if (_typeof(input) !== "object" || input === null) return input; var prim = input[Symbol.toPrimitive]; if (prim !== undefined) { var res = prim.call(input, hint || "default"); if (_typeof(res) !== "object") return res; throw new TypeError("@@toPrimitive must return a primitive value."); } return (hint === "string" ? String : Number)(input); }
var DataMap = exports.DataMap = /*#__PURE__*/function () {
  function DataMap(id, corpus, reads, glosses) {
    _classCallCheck(this, DataMap);
    _defineProperty(this, "_id", void 0);
    _defineProperty(this, "_corpus", void 0);
    _defineProperty(this, "_reads", void 0);
    _defineProperty(this, "_glosses", void 0);
    this._id = id;
    this._corpus = corpus;
    this._reads = reads;
    this._glosses = glosses;
  }

  // Getter and setter for reads
  _createClass(DataMap, [{
    key: "reads",
    get: function get() {
      return this._reads;
    }

    // Getter and setter for glosses
  }, {
    key: "glosses",
    get: function get() {
      return this._glosses;
    }
  }, {
    key: "corpus",
    get: function get() {
      return this._corpus;
    }
  }, {
    key: "incrementRelation",
    value: function incrementRelation(type) {
      var incAmount = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 1;
      if (type === RelationType.READ) this._reads += incAmount;else if (type === RelationType.GLOSSED) this._glosses += incAmount;
    }

    // Getter for id
  }, {
    key: "id",
    get: function get() {
      return this._id;
    }

    // Setter for id
    ,
    set: function set(value) {
      // You can add validation logic if needed
      this._id = value;
    }
  }]);
  return DataMap;
}();
var RelationType = exports.RelationType = /*#__PURE__*/function (RelationType) {
  RelationType["READ"] = "READ";
  RelationType["GLOSSED"] = "GLOSSED";
  return RelationType;
}({});