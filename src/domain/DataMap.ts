export class DataMap {
  private _id: number;
  private _corpus: string;
  private _reads: number;
  private _glosses: number;

  constructor(id: number, corpus: string, reads: number, glosses: number) {
    this._id = id;
    this._corpus = corpus;
    this._reads = reads;
    this._glosses = glosses;
  }

  // Getter and setter for reads
  get reads(): number {
    return this._reads;
  }

  // Getter and setter for glosses
  get glosses(): number {
    return this._glosses;
  }

  get corpus(): string {
    return this._corpus;
  }

  incrementRelation(type: RelationType, incAmount: number = 1) {
    if (type === RelationType.READ) this._reads += incAmount;
    else if (type === RelationType.GLOSSED) this._glosses += incAmount;
  }

  // Getter for id
  get id(): number {
    return this._id;
  }

  // Setter for id
  set id(value: number) {
    // You can add validation logic if needed
    this._id = value;
  }
}

export enum RelationType {
  READ = "READ",
  GLOSSED = "GLOSSED",
}
