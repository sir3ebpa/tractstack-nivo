import records from "../data/records";
import { SwarmPlotDatum } from "../components/SwarmPlotCustomCircle";
import { DataMap, RelationType } from "../domain/DataMap";

export interface IChartData {
  groups: string[];
  data: SwarmPlotDatum[];
}

export const NullChartData: IChartData = {
  groups: ["null"],
  data: [],
};

// just use existing data but stringify it to simulate a json response
// put data fetch here or whatever
const fetchData = async () => {
  return JSON.stringify(records);
};

const convertGraphData = (data: any[]): IChartData => {
  const dataMap = new Map<number, DataMap>();
  for (let i = 0; i < data.length; ++i) {
    const r = data[i].r;
    if (!r) continue;

    const c = data[i].c;
    const corpusName = c.properties.object_name ?? "UNKNOWN";

    let entry = dataMap.get(r.end);
    if (!entry) {
      entry = new DataMap(r.end, corpusName, 0, 0);
      dataMap.set(r.end, entry);
    }

    const relation: RelationType =
      RelationType[r.type as keyof typeof RelationType];
    console.log(relation);
    entry.incrementRelation(relation, 1);
  }

  const plotData: SwarmPlotDatum[] = [];
  dataMap.forEach((val, key) => {
    plotData.push({
      id: val.corpus,
      group: "group",
      price: val.reads + val.glosses,
      volume: val.reads + val.glosses,
      categories: [0, val.reads, val.glosses],
    });
  });

  return { groups: ["group"], data: plotData };
};

export const getData = async () => {
  const jsonData = await fetchData();
  const data = convertGraphData(JSON.parse(jsonData));
  console.log(JSON.stringify(data));
  return data;
};
