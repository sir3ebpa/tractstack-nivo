// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
import React, { useEffect, useState } from 'react'
import { Theme } from '@nivo/core'
import { generateSwarmPlotData } from '@nivo/generators'
import { ComputedDatum, SwarmPlot } from '@nivo/swarmplot'
import { CustomCircle } from "./CustomCircle";
import { getData, IChartData, NullChartData } from "../services/DataFetcher";

export type SwarmPlotDatum = ReturnType<typeof generateSwarmPlotData>['data'][number]

const ShadowsLayer = (nodes: ComputedDatum<SwarmPlotDatum>[]) => (
  <>
    {nodes.map(node => (
      <circle
        key={node.id}
        cx={node.x}
        cy={node.y + node.size * 0.2}
        r={node.size * 0.55}
        fill="rgba(45, 0, 75, .15)"
      />
    ))}
  </>
)

const theme: Theme = {
  background: 'rgb(216, 218, 235)',
  axis: {
    ticks: {
      line: {
        stroke: 'rgb(84, 39, 136)',
      },
      text: {
        fill: 'rgb(84, 39, 136)',
        fontWeight: 600,
      },
    },
    legend: {
      text: {
        fill: 'rgb(84, 39, 136)',
        fontSize: 15,
      },
    },
  },
  grid: {
    line: {
      stroke: 'rgb(128, 115, 172)',
      strokeDasharray: '2 4',
      strokeWidth: 2,
    },
  },
}

const generateRandomData = () => {
  const data = generateSwarmPlotData(['group'], { min: 32, max: 32, categoryCount: 9 });
  console.log(`Random data generated: ${JSON.stringify(data)}`);
  return data;
}

type ChartProps = {
  valueMin: number,
  valueMax: number,
  volumeValues: number[],
  volumeSizes: number[],
}

const MIN_VOLUME_NODE_SIZE: number = 10;
const MAX_VOLUME_NODE_EXTRA_SPACE_PCT: number = 0.05;

export const SwarmPlotCustomCircle = () => {
  const [chartProps, setChartProps] = useState<ChartProps>({
    valueMin: 0,
    valueMax: 25,
    volumeValues: [0, 85],
    volumeSizes: [5, 85]
  });
  const [data, setData] = useState<IChartData>(NullChartData);
  useEffect(() => {
    const fetchData = async () => {
      const data = await getData();
      setData(data);

      // sort by descending
      const volumeSortedData = data.data.sort((a, b) => b.volume - a.volume);
      const chartPropsCopy = { ...chartProps };
      const maxValue = volumeSortedData[0].volume;
      const minValue = volumeSortedData[volumeSortedData.length - 1].volume;
      chartPropsCopy.valueMax = maxValue;
      chartPropsCopy.valueMin = minValue;

      chartPropsCopy.volumeSizes = [MIN_VOLUME_NODE_SIZE, maxValue];

      // grab min node size but make sure it's never less than MIN_VOLUME_NODE_SIZE
      const volumeNodeSizeMin = Math.min(minValue, MIN_VOLUME_NODE_SIZE);
      const volumeNodeSizeMax = Math.max(volumeNodeSizeMin, (maxValue - (maxValue * MAX_VOLUME_NODE_EXTRA_SPACE_PCT)));
      chartPropsCopy.volumeValues = [volumeNodeSizeMin, volumeNodeSizeMax];
      setChartProps(chartPropsCopy);
    }
    fetchData();
  }, []);

  return (
    <div className="v-flex-panel, row-gap-m">
      <SwarmPlot
        width={1000}
        height={400}
        margin={{
          top: 30,
          right: 80,
          bottom: 80,
          left: 80,
        }}
        data={data.data}
        groups={data.groups}
        groupBy="group"
        id="id"
        value="price"
        valueScale={{
          type: 'linear',
          min: chartProps.valueMin,
          max: chartProps.valueMax,
        }}
        size={{
          key: 'volume',
          values: chartProps.volumeValues,
          sizes: chartProps.volumeSizes,
        }}
        spacing={12}
        enableGridY={false}
        axisTop={null}
        axisRight={null}
        axisLeft={null}
        axisBottom={{
          legend: `Hi Adon`,
          legendPosition: 'middle',
          legendOffset: 50,
        }}
        circleComponent={CustomCircle}
        layers={[
          'grid',
          'axes',
          self => ShadowsLayer(self.nodes),
          'circles',
          'annotations'
        ]}
        layout="horizontal"
        theme={theme}
        isInteractive={true}
        useMesh={true}
        tooltip={p => {
          return (
            <div
              style={{
                background: 'wheat',
                padding: '9px',
                border: '1px solid #ccc',
                display: 'flex',
                width: '100%',
                flexDirection: 'column',
                alignItems: 'flex-start'
              }}
            >
              <span>Page: <b>{p.data.id}</b></span>
              <span>Total Events: <b>{p.data.volume}</b></span>
              <span>Reads: <b>{p.data.categories?.at(1)}</b></span>
              <span>Glossed: <b>{p.data.categories?.at(2)}</b></span>
            </div>
          );
        }}
        onMouseEnter={e => console.log("enter: " + e.index)}
      />
      <button onClick={e => setData(generateRandomData())}>
        Randomize data
      </button>
    </div>
  )
}
