// eslint-disable-next-line @typescript-eslint/no-unused-vars, no-unused-vars
import React, { useCallback } from "react";
import { CircleProps } from "@nivo/swarmplot";
import { useTooltip } from "@nivo/tooltip";
import { useOrdinalColorScale } from "@nivo/colors";
import { usePie } from "@nivo/pie";
import { SwarmPlotDatum } from "./SwarmPlotCustomCircle";

//https://github.com/plouc/nivo/blob/master/packages/bar/src/BarItem.tsx
export const CustomCircle = (props: CircleProps<SwarmPlotDatum>) => {
  const { showTooltipFromEvent, hideTooltip } = useTooltip();

  const getArcColor = useOrdinalColorScale({ scheme: 'category10' }, v => v)
  const pie = usePie({
    data: props.node.data.categories?.map((value, id) => ({
      id,
      value,
      hidden: false,
      data: value,
      color: '',
      formattedValue: `${value}`,
      label: `${value}`,
    })) || [],
    radius: props.node.size / 2,
    innerRadius: (props.node.size / 2) * 0.7,
    sortByValue: true,
  });

  const handleMouseEnter = useCallback(
    (event: React.MouseEvent) => {
      props.onMouseEnter?.(props.node, event)
      //showTooltipFromEvent(renderTooltip(), event)
    },
    [props.node, props.onMouseEnter, showTooltipFromEvent, /*renderTooltip*/]
  );
  const handleMouseLeave = useCallback(
    (event: React.MouseEvent) => {
      props.onMouseLeave?.(props.node, event)
      hideTooltip()
    },
    [props.node, hideTooltip, props.onMouseEnter]
  );

  return (
    <g transform={`translate(${props.node.x},${props.node.y})`}
      onMouseEnter={handleMouseEnter}
      onMouseLeave={handleMouseLeave}>
      <circle r={props.node.size / 2} stroke="rgb(216, 218, 235)" strokeWidth={12} />
      <circle
        r={props.node.size / 2}
        fill="rgb(45, 0, 75)"
        stroke="rgb(45, 0, 75)"
        strokeWidth={6}
      />
      {pie.dataWithArc.map((datum, i) => {
        return <path key={i} d={pie.arcGenerator(datum.arc) || undefined} fill={getArcColor(i)} />
      })}
      {props.node.size > 52 && (
        <text fill="white"
          textAnchor="middle"
          dominantBaseline="central"
          style={{
            fontSize: 14,
            fontWeight: 800,
          }}>
          {props.node.value.toString()}
        </text>
      )}
    </g>
  )
}
